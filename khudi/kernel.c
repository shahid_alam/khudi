/*
;
; Filename:    kernel.c
; Author:      Shahid Alam
; Dated:       December 6, 2009
; Description: The Kernel. Following is the memory map
;              of different components loaded:
;
;   |--------------| 0x0000                 -------|---------------|
;   |              |                               |               |
;   |              | Lower memory = 31744 bytes    |               |
;   |              |                               |               |
;   |--------------| 0x7C00                        |               |
;   |              |                               |               |
;   | boot loader  | 512 bytes                     |               |
;   |              |                               |               |
;   |--------------| 0x7E00                        |               |
;   |              |                               |  50176 bytes  |
;   |   secondary  |                               |               |
;   |  boot loader | Maximum size = 0x4600 bytes   |               |
;   |    (SBL)     | = 35 x 512 = 17920 bytes      |               |  1 MB
;   |              |                               |               |
;   |              | Depends on the size of SBL    |               |
;   |    KERNEL    | and the kernel                |               |
;   |              |                               |               |
;   |              |                               |               |
;   |--------------| 0xC400                 -------|               |
;   |              |                                               |
;          ---                                                     |
;          ---        Memory for I/O including video               |
;          ---                                                     |
;   |              |                                               |
;   |--------------| 0x100000               -----------------------|
;   |              |
;   |              |
;   |              |
;          ---
;          ---        Rest of the memory
;          ---
;   |              |
;   |              |
;   |              |
;   |--------------|
;
*/

#include "kernel.h"

unsigned int device;

/*
 * Display char in teletype mode using DMA (direct memory access)
 */
static void putc(char c, unsigned int attr)
{
	if (COL >= 80) { ROW++, COL = 0; }
	if (c == '\n') { ROW++; COL = 0; }
	else { put_char(ROW, COL++, c | attr); }
}

/*
 * Display string on the screen
 */
static void puts(char *str, unsigned int attr)
{
	while (*str)
		putc(*str++, attr);
}

static void drawKhudi(void)
{
	unsigned int attr = GREEN_OVER_BLACK;

	puts("           ______\n", attr);
	puts("          | -  - |\n", attr);
	puts("          | .  . |\n", attr);
	puts("          |  --  |\n", attr);
	puts("          |__::__|\n", attr);
	puts("            |  |\n", attr);
	puts("       _____|  |_____\n", attr);
	puts("      |              |\n", attr);
	puts("      |   K H U D I  |\n", attr);
	puts("      |              |\n", attr);
	puts("      |     Self     |\n", attr);
	puts("      |   Reliance   |\n", attr);
	puts("      |              |\n", attr);
	puts("      |______________|\n", attr);
	puts("         |  |   |  |\n", attr);
	puts("         |  |   |  |\n", attr);
	puts("         |  |   |  |\n", attr);
	puts("         |  |   |  |\n", attr);
	puts("         |  |   |  |\n", attr);
	puts("         |  |   |  |\n", attr);
	puts("         |__|   |__|\n", attr);
	puts("\n");
}

/*
 * Append string src to dest starting from start
 *
 * It's much faster than C implementation of strcat
 * which checks the complete buf till it finds '\0'
 * and then it copies starting from there
 *
 * You have to count the length of the string yourself
 * I think we are letting computers take over so
 * this time I thought let's not be the slave of my computer
 *
 */
static unsigned int append(unsigned int start, char *dest, char *src)
{
	unsigned int j;
	for (j = 0; src[j] != '\0'; j++)
		dest[start+j] = src[j];
	start += j;
	dest[start] = '\0';
	return start;
}

/*
 * The main kernel function
 */
extern void kernel(void)
{
	char str[512];
	ROW = COL = 0;

	set_text_mode();
	hide_cursor();
	clear_screen();
	puts("Khudi (c) 2010 Successfully Booted. ", BWHITE_OVER_BLACK);
	append(0, str, "Testing copying and appending string.\n");
	puts(str, WHITE_OVER_BLACK);
	puts("Write your own OS (Khudi) and have Fun:\n", WHITE_OVER_BLACK);
	drawKhudi();

	halt();
}
