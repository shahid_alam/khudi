#ifndef _KERNEL_H
#define _KERNEL_H

#define GREEN_OVER_BLACK    0x0200          /* Green over black */
#define WHITE_OVER_BLACK    0x0700          /* White over black */
#define BWHITE_OVER_BLACK   0x0F00          /* Bright white over black */

/*
 * Variables shared with load2.s:
 */
extern unsigned int device;                 /* Drive being booted from */
static unsigned int ROW;
static unsigned int COL;

/*
 * Functions defined in utilsx386.s
 */

/*
 * Clears the screen for text mode 0
 */
extern void clear_screen(void);
/*
 * Send a character to the screen in text mode 0
 */
extern void put_char(unsigned row, unsigned col, unsigned int ch);
/*
 * Hide the cursor
 */
extern void hide_cursor(void);
/*
 * Set the text mode to 80x25 with 16 colors
 */
extern void set_text_mode(void);
/*
 * Halt the computer
 */
extern void halt(void);

#endif   /* _KERNEL_H */
