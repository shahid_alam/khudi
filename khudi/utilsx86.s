;
; Filename:    utilsx386.s
; Author:      Shahid Alam
; Datred:      December 6, 2009
; Description: Utilities for x86
;

[BITS 16]

   TEXT_ROWS           EQU    25        ; Number of total rows in text mode
   TEXT_COLS           EQU    80        ; Number of total cols in text mode

; -------------------------------------------------------------------------------------

;
; void put_char(u16_t row, u16_t col, u16_t char);
; Write a char with attr to display in text mode
; Parameters are passed on the stack
;
; Display Text memory @ B800:0000
;
; Offset computation:
; Offset = (row x (cols per row x 2)) + (col x 2)
;
global  _put_char
_put_char:
   mov  bx, sp            ; Save the stack pointer
   push es
   push di

   mov  ax, [bx+2]        ; Get the row
   mov  cl, (2*TEXT_COLS)
   mul  cl
   mov  dx, ax
   mov  ax, [bx+4]        ; Get the col
   mov  cl, 2
   mul  cl
   add  ax, dx
   mov  di, ax
   mov  ax, 0xB800
   mov  es, ax            ; es:di = B800:XXXX = display memory
   mov  ax, [bx+6]        ; Get the char and attr
   stosw                  ; Display the char with attr

   pop  di
   pop  es
   ret

;
; void set_text_mode(void);
; Set the text mode to 80x25 16 colors
; and cursor shape to blinking
;
;
global _set_text_mode
_set_text_mode:
   mov  al, 0x03
   mov  ah, 0x00
   int  0x10
   ret


;
; void hide_cursor(void);
; Hide the cursor in text mode using BIOS
;
;
global  _hide_cursor
_hide_cursor:
   mov  ch, 0x20
   mov  ah, 0x01
   int  0x10
   ret


;
; void clear_screen(void)
; Clears the text screen for text mode
;
global _clear_screen
_clear_screen:
   push es
   push di

   mov  ax, 0xB800
   mov  es, ax
   xor  di, di            ; es:di = B800:0000 = display memory
   xor  ax, ax
   mov  cx, (TEXT_ROWS*TEXT_COLS)
   rep  stosw             ; Clear the screen

   pop  di
   pop  es
   ret

;
; void halt(void)
;
;
global _halt
_halt:
   cli
   hlt
