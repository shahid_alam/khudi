;
; Filename:    load.s
; Author:      Shahid Alam
; Datred:      June 03, 2009
; Description: Loads Secondary Boot Loader and the Kernel from Floppy Drive
;              Loads at address BOOTSEG and then jump to that address
;

[BITS 16]
[ORG 0]

SECTION .text

START:
	jmp  startp

	LOADSEG          EQU    0x07C0   ; 0x7C00 is where this code is loaded
	BOOTSEG          EQU    0x07E0   ; Secondary boot code segment address
	BOOTOFF          EQU    0x0000   ;

	loadRebootMessage       db 'Press any key to reboot', 13, 10, 0
	loadBootMessage         db 'Loading Kernel (c) 2009', 13, 10, 0
	loadKernelMessage       db 'Kernel Loaded Successfully', 13, 10, 0

; -------------------------------------------------------------------------------------

reboot:
   lea  si, [loadRebootMessage]
   call printMessage
   mov  ah, 0           ; wait for key
   int  0x16

   db   0xea            ; machine language (opcodes) to jump to FFFF:0000 (reboot)
   dw   0x0000
   dw   0xffff

printMessage:
   lodsb                ; loads from DS:SI to AL and increment SI
   or   al, al          ; check if it's 0
   jz   donep           ; if it is then exit loop
   mov  ah, 0x0e        ; service of BIOS to teletype print with cursor advancement
   int  0x10
   jmp  printMessage    ; keep on looping until it's 0
donep:
   ret

; -------------------------------------------------------------------------------------

startp:
   mov  ax, LOADSEG
   mov  ds, ax
   mov  ax, 0x0000
   cli                  ; Ignore interrupts while setting stack
   mov  ss, ax
   mov  sp, ax          ; Usual place for a bootstrap stack
   sti

   mov  dl, 0           ; Drive = where we booted from, forcing floppy drive
	mov  bp, sp			   ; setting the base pointer to the stack pointer
   cld                  ; Clear the direction flag

   mov  si, loadBootMessage
   call printMessage

;
; Load second boot loader and the kernel of
; maximum size 17920 (35 * 512) from the
; boot device. It passes the boot device in
; DL to the second boot loader.
;
;
read:
   xor  ax, ax          ; reset the disk drive
   int  0x13
   jc   reboot          ; reboot on error

   mov  ax, BOOTSEG
   mov  es, ax
   xor  bx, bx

   mov  al, 35          ; Load 35 sectors
   mov  ah, 2           ; Load disk data to ES:BX
   mov  cl, 2           ; Starting Sector = 2
   mov  ch, 0           ; Track/Cylinder = 0
   mov  dl, 0           ; Drive = where we booted from, forcing floppy drive
   mov  dh, 0           ; Head = 0
   int  0x13            ; Read!
   jc   read            ; read again on error

readDone:

   mov  si, loadKernelMessage
   call printMessage

   jmp  BOOTSEG:BOOTOFF

   times 510-($-$$) db 0 ; Make the file 512 bytes long
   dw 0xAA55             ; Add the boot signature
