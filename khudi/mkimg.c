#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define SECTOR_SIZE         512           /* One sector on the disk */

int main (char argc, char *argv[])
{
	FILE *in, *out;
	char filename[1024];
	char file[1024];
	char *buffer[100], *ptr;
	int i, n, nf = 0, size[100], bsize = 0, numRead = 0, added = 0, numWrite = 0;
	int temp, len = -1;

	printf ("\n");
	if (argc > 4)
	{
		for (i = 0; i < argc-1; i++)
		{
			if (i+2 == argc-1 && strcmp(argv[i+1], "=") == 0)
			{
				len = sprintf (filename, "%s", argv[i+2]);
				filename[len] = '\0';
				break;
			}
			printf ("%2d -> File: %s\n", i+1, argv[i+1]);
			if ((in = fopen(argv[i+1], "rb")) == NULL)
			{
				printf("EROOR: loading file: %s\n", argv[i+1]);
				printf("Usage: mkimg [filenames --- more than 1] = [out filename]\n\n");
				for (i = 0; i < nf; i++)
					free (buffer[i]);
				fclose(in);
				fclose(out);
				return 1;
			}
			else
			{
				fseek(in, 0, SEEK_END);
				size[i] = ftell(in);
				bsize += size[i];
				rewind(in);
				nf++;
				ptr = strrchr(argv[i+1], '/');
				temp = ptr - argv[i+1] + 1;
				for (n = temp; n < strlen(argv[i+1]); n++)
					file[n-temp] = argv[i+1][n];
				file[n-temp] = '\0';
				added = 0; numRead = 0;

				if (size[i] < SECTOR_SIZE)
					added = SECTOR_SIZE - size[i];
				else if ((size[i] % SECTOR_SIZE) != 0)
					added = SECTOR_SIZE - (size[i] % SECTOR_SIZE);
				buffer[i] = (char *) malloc (sizeof(char)*(size[i]+added));
				for (n = 0; n < size[i]; n++)
				{
					buffer[i][n] = fgetc(in);
					numRead++;
				}
				for (n = size[i]; n < (size[i]+added); n++)
					buffer[i][n] = '\0';
				size[i] += added;
				bsize += added;

				fflush(in);
				printf ("      Size: %7d   Read: %7d   Added: %7d   Total Image Size: %7d\n", size[i], numRead, added, bsize);
				if (numRead <= 0)
				{
					printf ("ERROR: reading file '%s'\n\n", argv[i+1]);
					for (i = 0; i < nf; i++)
						free (buffer[i]);
					fclose(in);
					fclose(out);
					return 1;
				}
				fclose(in);
			}
		}
		if (len <= 0)
		{
			printf("Usage: mkimg [filenames --- more than 1] = [out filename]\n");
			for (i = 0; i < nf; i++)
				free (buffer[i]);
			fclose(in);
			fclose(out);
			return 1;
		}
		if ((out = fopen(filename, "wb")) == NULL)
		{
			printf("EROOR: opening file: %s\n", filename);
			printf("Usage: mkimg [filenames --- more than 1] = [out filename]\n\n");
		}
		else
		{
			printf ("Generating %s\n", filename);
			for (i = 0; i < nf; i++)
			{
				for (n = 0; n < size[i]; n++)
				{
					fputc(buffer[i][n], out);
					numWrite++;
				}
				fflush (out);
			}
			if (numWrite <= 0)
				printf ("\nERROR: writing file %s\n\n", filename);
			else
				printf ("File %s generated of size %d: Written %d bytes \n\n", filename, bsize, numWrite);
		}
	}
	else
		printf("Usage: mkimg [filenames --- more than 1] = [out filename]\n\n");

	fclose(out);
	for (i = 0; i < nf; i++)
		free (buffer[i]);

	return 0;
}
