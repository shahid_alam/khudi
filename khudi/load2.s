;
; Filename:    load2.s
; Author:      Shahid Alam
; Datred:      December 6, 2009
; Description: This file contains the startup and low level support
;              for the secondary boot program which calls the kernel.
;
;              The primary boot loader code supplies the following parameters
;              in registers:
;              dl = Boot-device, in our case it's always the floppy disk = 0x00
;

USE16

SECTION .text

global main, _main
main:
_main:
   mov  ax, LOADSEG
   mov  ds, ax                 ; ds = header
   mov  es, ax
   cli                         ; Ignore interrupts while stack is not build
   mov  ss, ax
   mov  ax, kernel_stack_top
   mov  sp, ax                 ; Set sp at the top of all that
   sti                         ; Stack ok now
   cld                         ; Set the direction flag

   mov  si, loadBootMessage
   call printMessage

   ;
   ; Copy primary boot parameters to variables
   ;
   xor  dh, dh
   mov  [_device], dx          ; Boot device (in our case 0x00 - floppy drive)

   call _kernel
   cli
   hlt

printMessage:
   lodsb                       ; loads from DS:SI to AL and increment SI
   or   al, al                 ; check if it's 0
   jz   donep                  ; if it is then exit loop
   mov  ah, 0x0e               ; service of BIOS to teletype print with cursor advancement
   int  0x10
   jmp  printMessage           ; keep on looping until it's 0
donep:
   ret

;
; Imported functions and variables
;
extern _kernel, _device

LOADSEG          EQU    0x07E0 ; Where this code is loaded.
loadBootMessage         db 'Booting Kernel Khudi (c) 2010', 13, 10, 0

SECTION .bss

kernel_stack:
resb 1024                      ; kernel stack
kernel_stack_top:
